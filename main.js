import App from './App'

// #ifndef VUE3
import Vue from 'vue'
Vue.config.productionTip = false

// 挂载全局footer组件
import footers from './components/footer/footer.vue'
Vue.component('footers', footers)

/* // 小程序分享
import share from './common/share.js'
Vue.mixin(share) */

// 全局验证登录
import login from './common/login.js'
Vue.mixin(login)

App.mpType = 'app'
const app = new Vue({
    ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif