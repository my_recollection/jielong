export default {
	data() {
		return {
			userinfo: {}
		}
	},
	onLoad(option) {
		console.log(option)
		if(option.id && option.shop_id){
			uni.setStorageSync('shop_id', option.shop_id)
			uni.setStorageSync('redirect_url', '/pages/goods/detail/detail?id='+option.id)
			/* uni.navigateTo({
				url:'/pages/goods/detail/detail?id='+option.id
			}) */
		}
		let userinfo = uni.getStorageSync('userinfo')
		let page = getCurrentPages()[0].$page.fullPath
		this.userinfo = userinfo
		/* if ((page != '/pages/index/index' || page != '/pages/login/login') && (userinfo.user_id == '' || userinfo.user_id == undefined)) {
			
			uni.navigateTo({
				url: '/pages/login/login'
			})
			return
		} */
	}
}
