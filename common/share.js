export default {
	data() {
		return {
			share: {
				title: '保定桓达·团批',
				path: '/pages/index/index',
				imageUrl: "/static/share_logo.png",
				query: ''
			}
		}
	},
	// 发送给朋友
	onShareAppMessage(res) {
		if(res.from == "button"){
			let shop_id = uni.getStorageSync('shop_id')
			let id = res.target.dataset.id
			let title = res.target.dataset.title
			let image = res.target.dataset.image
			return {
				title: title,
				path: '/pages/goods/detail/detail?id='+id+'&shop_id='+shop_id,
				imageUrl: image
			}
		}else{
			return {
				title: this.share.title,
				path: this.share.path,
				imageUrl: this.share.imageUrl
			}
		}
		
	}
}
