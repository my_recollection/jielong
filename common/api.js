import {
	request,
	uploadImg
} from '@/common/request.js'

// 登录
export function login(params = {}) {
	return request({
		url: '/api/users/wxLogin',
		method: 'get',
		params
	})
}

// 绑定手机号
export function bindWxPhone(params = {}) {
	return request({
		url: '/api/users/bindWxPhone',
		method: 'get',
		params
	})
}

// article 信息
export function article(params = {}) {
	return request({
		url: '/api/index/article',
		method: 'get',
		params
	})
}

// index 信息
export function getIndex(params = {}) {
	return request({
		url: '/api/index/index',
		method: 'get',
		params
	})
}

// 店铺列表
export function getShops(params = {}) {
	return request({
		url: '/api/index/shops',
		method: 'get',
		params
	})
}


// 店铺详情
export function getShopinfo(params = {}) {
	return request({
		url: '/api/index/shopinfo',
		method: 'get',
		params
	})
}

// 商品列表
export function getGoods(params = {}) {
	return request({
		url: '/api/product/lists',
		method: 'get',
		params
	})
}

// 上传
export function upload(params = {}) {
	return uploadImg({
		url: '/api/index/upload',
		method: 'get',
		params
	})
}


// 商品详情
export function getGoodsInfo(params = {}) {
	return request({
		url: '/api/product/detail',
		method: 'get',
		params
	})
}

// 商品详情
export function quxiao(params = {}) {
	return request({
		url: '/api/product/quxiao',
		method: 'get',
		params
	})
}

// 商品详情购买列表
export function getOrders(params = {}) {
	return request({
		url: '/api/product/orders',
		method: 'get',
		params
	})
}

// 下单购买
export function buy(params = {}) {
	return request({
		url: '/api/product/order',
		method: 'get',
		params
	})
}

// 获取商品单位列表
export function getUnits(params = {}) {
	return request({
		url: '/api/product/units',
		method: 'get',
		params
	})
}

// 发布商品
export function publish(params = {}) {
	return request({
		url: '/api/product/publish',
		method: 'get',
		params
	})
}

// 我的发布记录
export function getMyProducts(params = {}) {
	return request({
		url: '/api/product/myproducts',
		method: 'get',
		params
	})
}


// 商品详情(修改或下单时用)
export function getProInfo(params = {}) {
	return request({
		url: '/api/product/proinfo',
		method: 'get',
		params
	})
}

// 统计页面上部分
export function getTotalInfo(params = {}) {
	return request({
		url: '/api/product/totalinfo',
		method: 'get',
		params
	})
}

// 订单详情
export function getOrderInfo(params = {}) {
	return request({
		url: '/api/product/orderinfo',
		method: 'get',
		params
	})
}

// 修改订单数量
export function editNum(params = {}) {
	return request({
		url: '/api/product/editNum',
		method: 'get',
		params
	})
}

// 获取会员信息
export function getUserInfo(params = {}) {
	return request({
		url: '/api/users/info',
		method: 'get',
		params
	})
}

// 获取会员信息
export function getPhone(params = {}) {
	return request({
		url: '/api/index/kefu',
		method: 'get',
		params
	})
}