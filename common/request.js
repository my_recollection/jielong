import {
	baseUrl
} from "@/common/config.js"

// 获取请求头
function getHeaders() {
    let header = {
		"token": 'token',
        "Content-Type": "application/json", //根据自己的数据类型
    }
    return header
}

export function request(options) {
	options.method = options.method?options.method.toUpperCase():'GET'
	if (!['GET', 'POST', 'PUT', 'DELETE'].includes(options.method)) {
		uni.showToast({
			title: `暂不支持的请求方式: ${options.method}`,
			icon: 'none'
		});
		return
	}
	if(uni.getStorageSync('userinfo').user_id){
		options.params.user_id = uni.getStorageSync('userinfo').user_id
	}
	if(uni.getStorageSync('userinfo').token){
		options.params.token = uni.getStorageSync('userinfo').token
	}
	options.params.shop_id = 5
	return new Promise((resolve, reject) => {
		uni.request({
			url: baseUrl + options.url,
			method: options.method,
			data: options.params,
			header: getHeaders(),
		}).then(res => {
			uni.hideLoading()
			switch (res[1].statusCode) {
				case 200:
					resolve(res[1].data)
					break
				default:
					reject(res)
					break
			}
		}).catch(
			(response) => {
				uni.hideLoading()
				reject(response)
			}
		)
	})
}

export function uploadImg(options) {
	options.method = options.method?options.method.toUpperCase():'GET'
	if (!['GET', 'POST', 'PUT', 'DELETE'].includes(options.method)) {
		uni.showToast({
			title: `暂不支持的请求方式: ${options.method}`,
			icon: 'none'
		});
		return
	}
	
	return new Promise((resolve, reject) => {
		uni.uploadFile({
			url: baseUrl + options.url,
			filePath: options.params.img,
			name: 'img',
			data: options.params,
			header: getHeaders(),
		}).then(res => {
			uni.hideLoading()
			switch (res[1].statusCode) {
				case 200:
					resolve(res[1].data)
					break
				default:
					reject(res)
					break
			}
		}).catch(
			(response) => {
				uni.hideLoading()
				reject(response)
			}
		)
	})
}